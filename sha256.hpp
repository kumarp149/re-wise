#ifndef __SHA256__HEADER__
#define __SHA256__HEADER__

#include <iostream>
#include <bits/stdc++.h>
#include <fstream>
#include <openssl/sha.h>

using namespace std;

string string_sha(string s);
string file_sha(string path);
bool valid_sha(string s);

#endif