#include "timer.hpp"


unsigned long get_milli(){
    unsigned long milliseconds_since_epoch = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
    return milliseconds_since_epoch;
}

int random_generator(){
    return rand()%(__RANDOM__CAP__) + __RANDOM__OFFSET__;
}