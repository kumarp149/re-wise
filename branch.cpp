#include "branch.hpp"


string get_branch(){
    ifstream ifs(".store/HEAD");
    if (!ifs){
        cerr << "probably the repo is tampered. Please type \"out reinit --help\" to know more" << endl;
        return "";
    }
    string ret;
    ifs >> ret;
    return ret;
}

void get_all_branches(unordered_set<string>& branch_list){
    string path = ".store/branches/";
    for (auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it){
        string filename = it->path().string();
        int pos = filename.length()-1;
        while(pos>=0 && filename[pos]!='/'){
            pos--;
        }
        filename = filename.substr(pos+1);
        branch_list.insert(filename);
    }
    return;
}

int delete_branch(string branch){
    return 0;
}