#ifndef __COLORS__HEADER__
#define __COLORS__HEADER__

#define NC "\e[0m"
#define RED "\e[0;31m"
#define GREEN "\e[0;32m"
#define CYAN "\e[0;36m"
#define REDB "\e[41m"

#endif