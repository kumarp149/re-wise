#ifndef __FS_HEADER__
#define __FS_HEADER__

#include <fstream>
#include <iostream>
#include <bits/stdc++.h>
#include <filesystem>
#include "sha256.hpp"
#include "include/gzip/gzip.hpp"
#include <sstream>
#include "colors.hpp"

#define __FILE__ERROR__ "FILE NOT FOUND"

#define __REPO__ERROR__ "probably the repo is tampered"

#define __FILE__SYSTEM__ERROR__ "problem performing the task, try again later"

using namespace std;

namespace fs = std::filesystem;

using namespace fs;

using namespace gzip;


void init_repo(string name,bool flag);

bool is_repo();

int replace_content(string path,string content);

//string read_file(string path);

string get_user();

bool file_exist(string path);

int get_tree_blobs(string tree_hash,unordered_map<string,string>& tree_blobs);

int get_working_blobs(string path,unordered_map<string,string>& working_blobs);

string file_content(string path);

void show_changes(string path,bool& first_commit,string latest_commit,int& changes);

string compress_string(string& s);

void show_error(string s);

void show_success(string prefix,string postfix);

void getTreeAndParent(vector<string>& vec,string& latest_hash);

int create_blob_file(unordered_map<string,string>& working_blobs,string path);

bool clean_repo();

string decompress_string(string s);

void change_tree(string from,string to);

#endif