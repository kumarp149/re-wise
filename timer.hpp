#ifndef __TIMER_HEADER__
#define __TIMER_HEADER__


#include <chrono>
#include "sha256.hpp"
#include <time.h>

#define __RANDOM__CAP__ 99999999
#define __RANDOM__OFFSET__ 1000

using namespace std::chrono;

unsigned long get_milli();

int random_generator();

#endif