#include "commit.hpp"

void create_first_commit(string path,string& commit_sha,string& tree_sha,ostringstream& lines,int& count_changes){
    for (auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it){
        if (it->symlink_status().type() == file_type::directory && it->path().string()!="./.store"){
            create_first_commit(it->path().string(),commit_sha,tree_sha,lines,count_changes);
        }
        else if (it->path().string()=="./.store"){
            continue;
        }
        else{
            string sha = file_sha(it->path().string());
            ifstream ist(".store/blobs/" + sha.substr(0,2) + "/" + sha);
            if (!ist){
                string content = file_content(it->path().string());
                string compressed_content = compress_string(content);
                if (!exists(".store/blobs/" + sha.substr(0,2) + "/")){
                    create_directory(".store/blobs/" + sha.substr(0,2) + "/");
                }
                ofstream ofs(".store/blobs/" + sha.substr(0,2) + "/" + sha);
                ofs << compressed_content;
                ofs.close();
            }
            ist.close();
            commit_sha = string_sha(commit_sha + sha + to_string(random_generator()));
            tree_sha = string_sha(tree_sha + sha + to_string(random_generator()));
            string to_tree = sha + it->path().string();
            lines << endl << to_tree;
            count_changes++;
            //cout << GREEN << "Untracked file: " << NC << it->path().string() << endl;
        }
    }
}

int create_object_file(string type,string& hash,string& data){
    string prefix = hash.substr(0,2);
    if (type == "commit"){
        if (exists(".store/commits/" + hash.substr(0,2) + "/")){

        }
        else{
            create_directory(".store/commits/" + hash.substr(0,2) + "/");
        }
        int val = replace_content(".store/commits/" + hash.substr(0,2) + "/" + hash,data);
        if (val<0){
            return -1;
        }
        else{
            return 0;
        }
    }
    else if (type == "tree"){
        if (exists(".store/trees/" + hash.substr(0,2) + "/")){

        }
        else{
            create_directory(".store/trees/" + hash.substr(0,2) + "/");
        }
        int val = replace_content(".store/trees/" + hash.substr(0,2) + "/" + hash,data);
        if (val<0){
            return -1;
        }
        else{
            return 0;
        }
    }
    return 0;
}

int delete_object_file(string &commit,string& tree){
    string commit_file = ".store/commits/" + commit.substr(0,2) + "/" + commit;
    if (exists(commit_file)){
        if (remove(commit_file) == false){
            return -1;
        }
    }
    string tree_file = ".store/trees/" + tree.substr(0,2) + "/" + tree;
    if (exists(tree_file)){
        if (remove(tree_file) == false){
            return -1;
        }
    }
    return 0;
}