#include "sha256.hpp"

string string_sha(string s){
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256,s.c_str(),s.size());
    SHA256_Final(hash,&sha256);
    stringstream ss;
    for (int i=0; i<SHA256_DIGEST_LENGTH; i++){
        ss << hex << setw(2) << setfill('0') << (int)hash[i];
    }
    return ss.str();
}

string file_sha(string path){
   ifstream fp(path, std::ios::in | std::ios::binary);
   if (!fp){
       return "";
   }
   constexpr const std::size_t buffer_size { 1 << 12 };
   char buffer[buffer_size];
   unsigned char hash[SHA256_DIGEST_LENGTH] = { 0 };
   SHA256_CTX ctx;
   SHA256_Init(&ctx);
   while(fp.good()){
       fp.read(buffer, buffer_size);
       SHA256_Update(&ctx, buffer, fp.gcount());
   }
   SHA256_Final(hash, &ctx);
   fp.close();
   std::ostringstream os;
   os << std::hex << std::setfill('0');
   for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i){
       os << std::setw(2) << static_cast<unsigned int>(hash[i]);
   }
   return os.str();
}

bool valid_sha(string s){
    if (s.length()!=64){
        return false;
    }
    for (auto x:s){
        int asci = int(x);
        if (48<=asci && asci<=57){
            continue;
        }
        else if (97<=asci && asci<=102){
            continue;
        }
        return false;
    }
    return true;
}