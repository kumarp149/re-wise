UNAME := $(shell uname)

ifeq ($(UNAME), CYGWIN_NT-10.0)
    OUT := rw.exe
    CC := g++
	INCLUDE := -I"C:/Users/Sruteesh Kumar/Desktop/zlib-1.2.11/zlib-1.2.11"
	LIB := -L"D:/ZipsAndTars/boost_1_78_0/boost_1_78_0/stage/lib" -L"C:/Users/Sruteesh Kumar/Desktop/zlib-1.2.11/zlib-1.2.11"
endif

ifeq ($(UNAME), Linux)
	CC := g++-9
	OUT := rw.out
endif

PROGRAM_OPTIONS := -lboost_program_options
C++_STANDARD = -std=c++17
SOURCES = $(wildcard *.cpp)
HEADERS = $(patsubst %.cpp,%.hpp,$(SOURCES))
OBJ = $(patsubst %.cpp,bin/%.o,$(SOURCES))
LINKER_FLAGS = -lstdc++fs -lz -lssl -lcrypto


$(OUT): bin/main.o $(OBJ)
	$(CC) -g $(C++_STANDARD) $(LIB) $^ -o $@ $(LINKER_FLAGS)

bin/main.o: main.cpp
	$(CC) -g $(C++_STANDARD) $(INCLUDE) -c -o $@ $^

bin/%.o: %.cpp
	$(CC) -g $(C++_STANDARD) $(INCLUDE) -c -o $@ $^

clean:
	rm -rf bin/*.exe
	rm -rf bin/*.exe.stackdump
	rm -rf bin/*.o
	rm -rf *.exe
	rm -rf *.exe.stackdump

delete: 
	rm -rf .store

run: 
	bin/out.exe status