#ifndef __COMMIT_HEADER__
#define __COMMIT_HEADER__

#include <fstream>
#include <iostream>
#include <bits/stdc++.h>
#include <filesystem>
#include "sha256.hpp"
#include "fs.hpp"
#include "timer.hpp"
#include "colors.hpp"

#define __CLEAN__DIRECTORY__ "There are no changes to commit in the working directory"

using namespace std;

namespace fs = std::filesystem;

using namespace fs;

void create_first_commit(string path,string& commit_sha,string& tree_sha,ostringstream& lines,int& count_changes);

int create_object_file(string type,string &commit_hash,string &final_tree_sha);

int delete_object_file(string &commit,string& tree);

#endif