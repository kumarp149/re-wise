#ifndef __ARG__HEADER__

#define __ARG__HEADER__

#define __VALUE__FLAG__(command,flag,help) (flag + " is a value option for " + command + " command. " + help) 
#define __UNKNOWN__(command,option) ("unknown option " + option + " provided for " + command + " command")
#define __MANY__(command) ("too many options provided for " + command + " command")
#define __NO__FLAGS__(command) ("no flags provided for " + command + " command")
#define __NON_VALUE__(command,option) (option + " is a non-value flag for " + command + " command")

#endif