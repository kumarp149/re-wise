#include <iostream>
#include <bits/stdc++.h>
#include <filesystem>
#include "include/gzip/gzip.hpp"
#include <time.h>
#include "colors.hpp"
#include "arg.hpp"

namespace fs = std::filesystem;

using namespace fs;

using namespace std;

using namespace gzip;

#include "sha256.hpp"
#include "fs.hpp"
#include "branch.hpp"
#include "timer.hpp"
#include "commit.hpp"
#include "stage.hpp"

#define VERSION "0.01"

#define HELP show_help_message(command)

bool valid_user_name(string& name){
    if (name.size()<5 || name.size()>15){
        return false;
    }
    for (auto x:name){
        int asci = int(x);
        if ((asci>=65 && asci<=90) || (asci>=97 && asci<=122)){
            continue;
        }
        else{
            return false;
        }
    }
    return true;
}

void show_help_message(string& cmd){
    if (cmd == "init"){
        cerr << "Usage: rw init [flag] [value]" << endl;
        cerr << endl;
        cerr << "flags:" << endl;
        cerr << "-h --help" << "\t" << "display this help [non-value flag]" << endl;
        cerr << "-v --version" << "\t" << "display the version [non-value flag]" << endl;
        cerr << "-u --user" << "\t" << "name of user required to initialize repo [value required]" << endl;
    }
    else if (cmd == "commit"){
        cerr << "Usage: rw commit [flag] [value]" << endl;
        cerr << endl;
        cerr << "flags:" << endl;
        cerr << "-h --help" << "\t" << "display this help [non-value flag]" << endl;
        cerr << "-m --message" << "\t" << "commit message" << endl;
    }
    else if (cmd == "stash"){
        cerr << "Usage: rw stash [argument]" << endl;
        cerr << endl;
        cerr << "arguments:" << endl;
        cerr << "apply" << "\t" << "apply the most recent stash" << endl;
        cerr << "pop" << "\t" << "apply the most recent stash and remove it from the existing stashes" << endl;
        cerr << "push" << "\t" << "create a stash" << endl;
        cerr << "drop" << "\t" << "just remove the stash from the existing stashes" << endl;
    }
    else if (cmd == "branch"){
        cerr << "Usage: out branch [flag] [value]" << endl;
        cerr << endl;
        cerr << "flags:" << endl;
        cerr << "-h --help" << "\t" << "display the help [non-flag value]" << endl;
        cerr << "-b" << "\t" << "create a new branch and checkout" << endl;
        cerr << "-c" << "\t" << "checkout to an existing branch" << endl;
    }
    else if (cmd == "reinit"){
        cerr << "Usage: rw reinit [flag] [value]" << endl;
        cerr << "WARNING: using this will delete all the existing commits, branches etc. so, use this only when the repo is tampered" << endl;
        cerr << "-h --help" << "\t" << "display this help [non-value flag]" << endl;
        cerr << "-v --version" << "\t" << "display the version [non-value flag]" << endl;
        cerr << "-u --user" << "\t" << "name of user required to initialize repo [value required]" << endl;
    }
}

int main(int argc, char *argv[]) {
    srand(time(0));
    if (argc<=1){
        cout << "Please provide an argument" << endl;
        return 0;
    }

    string command = string(argv[1]);

    //INIT_COMMAND

    if (command == "init"){
        if (argc<=2){  /*If number of arguments are less than 2, then no user name is provided and so error*/
            show_error(__NO__FLAGS__(command));
            cerr << endl;
            HELP;
        }
        else if (argc==3){
            /* If only 3 arguments are provided
             * only help option can be valid in this case 
             * All other commands/options will show error
             * If the last option is "-u" or "--user", then username is not provided and so error
            */
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                HELP;
            }
            else if (string(argv[2]) == "--user" || string(argv[2]) == "-u"){
                show_error(__VALUE__FLAG__(command,string(argv[2]),""));
                HELP;
            }
            else{
                show_error(__UNKNOWN__(command,string(argv[2])));
                HELP;
            }
        }
        else if (argc==4){
            /* If only 4 arguments are provided
             * help options should throw error
             * If the third option is "-u" or "--user", then the last option is username. validate it and init repo
             * Third option can only be "-u" or "--user"
            */
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                show_error(__NON_VALUE__(command,string(argv[2])));
                HELP;
            }
            else if (string(argv[2]) == "--user" || string(argv[2]) == "-u"){
                string name = string(argv[3]);
                if (valid_user_name(name) == false){
                    show_error("username should be comprised of [5-15] alphabet");
                    return 0;
                }
                else{
                    //This is the only correct way for init command
                    init_repo(name,false);
                    return 0;
                }
            }
            else{
                show_error(__UNKNOWN__(command,string(argv[2])));
                return 0;
            }
        }
        else if (argc>=5){
            /* If only 5 arguments are provided
             * In any case, error should be thrown
            */
            show_error(__MANY__(command));
            HELP;
        }
    }

    //COMMIT_COMMAND

    else if (command == "commit"){
        if (argc<=2){
            /* If only 2 arguments are provided
             * If there are only 2 args, then there is no commit message and so error
            */
            show_error(__NO__FLAGS__(command));
            cerr << endl;
            HELP;
        }
        else if (argc==3){
            /* If only 3 arguments are provided
             * only help option can be valid in this case
             * All other commands/options will show error
             * If the last option is "-m" or "--message", then commit message is not provided and so error
            */
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                HELP;
            }
            else if (string(argv[2]) == "--message" || string(argv[2]) == "-m"){
                show_error(__VALUE__FLAG__(command,string(argv[2]),"It specifies the commit message"));
                HELP;
            }
            else{
                show_error(__UNKNOWN__(command,string(argv[2])));
                HELP;
            }
        }
        else if (argc==4){
            /* If only 4 arguments are provided
             * help option should throw error
             * If the third option is "-m" or "--message", then validate the commit
             * All other options should throw error
            */
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                show_error(__NON_VALUE__(command,string(argv[3])));
                HELP;
            }
            else if (string(argv[2]) == "--message" || string(argv[2]) == "-m"){
                string message = string(argv[3]);
                if (message.length() > 20){
                    show_error("commit message cannot have more than 20 characters");
                    return 0;
                }
                else if (message[0]=='-'){
                    show_error("commit message is not a flag, it's a value");
                    return 0;
                }
                else{
                    if (is_repo() == false){
                        show_error("not a valid repo");
                        return 0;
                    }
                    //correct one
                    string branch = file_content(".store/HEAD");
                    if (branch == ""){
                        bool first_commit = true;
                        string user_name = file_content(".store/user");
                        if (user_name == __FILE__ERROR__){
                            show_error(__REPO__ERROR__);
                            return 0;
                        }
                        string final_commit_sha = string_sha(to_string(random_generator()) + to_string(get_milli()) + user_name + message);
                        string final_tree_sha = string_sha(to_string(random_generator()) + to_string(get_milli()) + message);
                        ostringstream lines;
                        int count_changes = 0;
                        create_first_commit(".",final_commit_sha,final_tree_sha,lines,count_changes);
                        if (count_changes==0){
                            cout << __CLEAN__DIRECTORY__ << endl;
                            return 0;
                        }
                        string to_tree = lines.str();
                        int val = create_object_file("commit",final_commit_sha,final_tree_sha);
                        if (val<0){
                            delete_object_file(final_commit_sha,final_tree_sha);
                            return 0;
                        }
                        val = create_object_file("tree",final_tree_sha,to_tree);
                        if (val<0){
                            delete_object_file(final_commit_sha,final_tree_sha);
                            return 0;
                        }
                        val = replace_content(".store/HEAD","main");
                        if (val<0){
                            delete_object_file(final_commit_sha,final_tree_sha);
                            return 0;
                        }
                        val = replace_content(".store/branches/main",final_commit_sha);
                        if (val<0){
                            delete_object_file(final_commit_sha,final_tree_sha);
                            return 0;
                        }
                        cout << "commit " << final_commit_sha << " done" << endl;
                        cout << count_changes << " total changes" << endl;
                        return 0;
                    }
                    else if (branch == __FILE__ERROR__){
                        show_error(__REPO__ERROR__);
                        return 0;
                    }
                    else{
                        string latest_hash = file_content(".store/branches/"+branch);
                        if (latest_hash.length() != 64){
                            show_error(__REPO__ERROR__);
                            return 0;
                        }
                        vector<string> vec;
                        getTreeAndParent(vec,latest_hash);
                        if (vec.size() == 0){
                            return 0;
                        }
                        string tree = vec[0];
                        string parent = (vec.size() == 2) ? (vec[1]) : ("");
                        bool first_commit = false;
                        unsigned long long int total_changes = 0;
                        unordered_map<string,string> tree_blobs, working_blobs;
                        int val = get_tree_blobs(tree,tree_blobs);
                        if (val < 0){
                            show_error(__FILE__SYSTEM__ERROR__);
                            return 0;
                        }
                        get_working_blobs(".",working_blobs);
                        bool changes_there = false;
                        string user_name = file_content(".store/user");
                        string final_commit_sha = string_sha(parent + user_name + to_string(get_milli()) + to_string(random_generator()));
                        string final_tree_sha = string_sha(tree + to_string(random_generator()));
                        ostringstream lines;
                        for (auto x:tree_blobs){
                            if (valid_sha(x.second) == false){
                                show_error(__REPO__ERROR__);
                                return 0;
                            }
                            if (working_blobs.find(x.first) != working_blobs.end()){
                                if (valid_sha(working_blobs[x.first]) == false){
                                    show_error(__REPO__ERROR__);
                                    return 0;
                                }
                                if (working_blobs[x.first] != x.second){      //File is modified
                                    total_changes++;
                                    string hash = working_blobs[x.first];
                                    final_commit_sha = string_sha(final_commit_sha + hash + to_string(random_generator()));
                                    final_tree_sha = string_sha(final_tree_sha + hash + to_string(random_generator()));
                                    changes_there = true;
                                    string temp = working_blobs[x.first] + x.first;
                                    lines << endl << temp;
                                    create_blob_file(working_blobs,x.first);
                                }
                                else{
                                    string temp = x.second + x.first;
                                    lines << endl << temp;
                                }
                                working_blobs.erase(x.first);
                            }
                            else{       // File is deleted
                                changes_there = true;
                                final_commit_sha = string_sha(final_commit_sha + to_string(random_generator()));
                                final_tree_sha = string_sha(final_tree_sha + to_string(random_generator()));
                                total_changes++;
                            }
                        }
                        for (auto x:working_blobs){   //File is created
                            total_changes++;
                            string hash = x.second;
                            final_commit_sha = string_sha(final_commit_sha + hash + to_string(random_generator()));
                            final_tree_sha = string_sha(final_tree_sha + hash + to_string(random_generator()));
                            changes_there = true;
                            string temp = x.second + x.first;
                            lines << endl << temp;
                            create_blob_file(working_blobs,x.first);
                        }
                        if (changes_there == false){
                            cout << __CLEAN__DIRECTORY__ << endl;
                            return 0;
                        }
                        else{
                            string to_tree = lines.str();
                            create_directory(".store/commits/" + final_commit_sha.substr(0,2) + "/");
                            create_directory(".store/trees/" + final_tree_sha.substr(0,2) + "/");
                            replace_content(".store/trees/" + final_tree_sha.substr(0,2) + "/" + final_tree_sha,to_tree);
                            ofstream of(".store/commits/" + final_commit_sha.substr(0,2) + "/" + final_commit_sha);
                            of << final_tree_sha << endl;
                            of << latest_hash;
                            of.close();
                            cout << "commit " << final_commit_sha << " done" << endl;
                            cout << total_changes << " changes";
                            replace_content(".store/branches/" + branch,final_commit_sha);
                            return 0;
                        }
                    }
                }
            }
            else{
                show_error(__UNKNOWN__(command,string(argv[3])));
                return 0;
            }
        }
        else if (argc>=5){
            show_error(__MANY__(command));
            HELP;
        }
    }


    //STASH_COMMAND

    else if (command == "stash"){
        if (argc==2){
            //correct
        }
        else if (argc==3){
            string arg1 = string(argv[2]);
            if (arg1 == "apply"){
                //correct
            }
            else if (arg1 == "pop"){

            }
            else if (arg1 == "push"){

            }
            else if (arg1 == "drop"){

            }
            else if (arg1 == "--help" || arg1 == "-h"){
                HELP;
            }
            else{
                show_error(__UNKNOWN__(command,string(arg1)));
                HELP;
            }
        }
        else if (argc>=4){
            show_error(__MANY__(command));
            HELP;
        }
    }


    //BRANCH_COMMAND

    else if (command == "branch"){
        if (argc==2){
            string branch_name = file_content(".store/HEAD");
            unordered_set<string> branch_list;
            get_all_branches(branch_list);
            if (branch_name == ""){
                return 0;
            }
            if (branch_list.size() == 0){
                return 0;
            }
            for (auto x:branch_list){
                if (x == branch_name){
                    cout << GREEN << "*" << x << NC << endl;
                }
                else{
                    cout << x << endl;
                }
            }
            return 0;
        }
        else if (argc==3){
            string arg1 = string(argv[2]);
            if (arg1 == "-h" || arg1 == "--help"){
                HELP;
            }
            else if (arg1 == "-c" || arg1 == "-b"){
                show_error(__VALUE__FLAG__(command,string(arg1),""));
                HELP;
            }
            else{
                show_error(__UNKNOWN__(command,string(arg1)));
                HELP;
            }
        }
        else if (argc==4){
            string arg1 = string(argv[2]);
            string arg2 = string(argv[3]);
            if (arg1 == "-h" || arg1 == "--help"){
                show_error(__NON_VALUE__(command,string(arg1)));
                HELP;
            }
            else if (arg1 == "-c"){
                if (arg2[0] == '-'){
                    show_error("branch name is not a flag, it's a value");
                    return 0;
                }
                else if (arg2.length() > 15){
                    show_error("error: branch name can have a maximum of 15 characters");
                    return 0;
                }
                else if (arg2.length() < 3){
                    show_error("error: branch name should have a minimum of 3 characters");
                    return 0;
                }
                if (clean_repo() == true){
                    unordered_set<string> branch_list;
                    get_all_branches(branch_list);
                    if (branch_list.find(arg2) != branch_list.end()){
                        show_error("a branch already exists with this name. choose a new name");
                        return 0;
                    }
                    string cur_branch = file_content(".store/HEAD");
                    string cur_hash = file_content(".store/branches/" + cur_branch);
                    replace_content(".store/HEAD",arg2);
                    replace_content(".store/branches/" + arg2,cur_hash);
                    cout << "created new branch " << arg2 << " at " << cur_hash.substr(0,8) << " and checked it out" << endl; 
                    return 0;
                }
                else{
                    show_error("there are some uncommitted changes in the directory. commit or stash them and checkout");
                    return 0;
                }
            }
            else if (arg1 == "-b"){
                if (arg2[0] == '-'){
                    show_error(__VALUE__FLAG__(command,string(arg1),""));
                }
                if (clean_repo() == false){
                    show_error("there are some uncommitted changes in the directory. commit or stash them and checkout");
                    return 0;
                }
                unordered_set<string> branch_list;
                get_all_branches(branch_list);
                string cur_branch = file_content(".store/HEAD");
                string cur_hash = file_content(".store/branches/" + cur_branch);
                if (cur_branch == arg2){
                    cout << "already in the same branch" << endl;
                    return 0;
                }
                else if (branch_list.find(arg2) == branch_list.end()){
                    show_error("target branch not found");
                    return 0;
                }
                else{
                    change_tree(cur_branch,arg2);
                    return 0;
                }
            }
        }
    }

    //REINIT_COMMAND

    else if (command == "reinit"){
        if (argc<=2){
            cerr << "error: unknown flags provided" << endl;
            cerr << endl;
            HELP;
        }
        else if (argc==3){
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                HELP;
            }
            else if (string(argv[2]) == "--user" || string(argv[2]) == "-u"){
                cerr << "error: " << string(argv[2]) << " is a value flag for reinit command. It specifies the name of the user" << endl;
                HELP;
            }
            else{
                cerr << "unknown option " << argv[2] << " for reinit command" << endl;
                HELP;
            }
        }
        else if (argc==4){
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){
                cerr << "error: unknown option " << argv[3] << " for reinit command" << endl;
                HELP;
            }
            else if (string(argv[2]) == "--user" || string(argv[2]) == "-u"){
                string name = string(argv[3]);
                if (valid_user_name(name) == false){
                    cerr << "error: username should be comprised of [5-15] alphabet" << endl; 
                }
                else{
                    init_repo(name,true);
                }
            }
        }
        else if (argc==5){
            cerr << "Too many options provided" << endl;
            HELP;
        }
    }

    //STATUS_COMMAND

    else if (command == "status"){
        if (argc==2){
            if (is_repo() == false){
                cerr << "not a valid repo" << endl;
                return 0;
            }
            string branch = file_content(".store/HEAD");
            if (branch == "ABC"){
                cerr << __REPO__ERROR__ << endl;
                return -1;
            }
            else if (branch == ""){
                bool first_commit = true;
                int changes = 0;
                for (auto it = fs::directory_iterator("."); it != fs::directory_iterator(); ++it){
                    if (it->symlink_status().type() == file_type::directory && it->path().string()!="./.store"){
                        show_changes(it->path().string(),first_commit,"",changes);
                    }
                    else if (it->path().string()=="./.store"){
                        continue;
                    }
                    else{
                        changes++;
                        cout << GREEN << "Untracked File:" << NC << "\t" << it->path().string().substr(2) << endl;
                    }
                }
                if (changes==0){
                    cout << "working directory is clean" << endl;
                    return 0;
                }
                else if (changes==1){
                    cout << "total " << changes << " untracked change" << endl;
                    return 0;
                }
                else{
                    cout << "total " << changes << " untracked changes" << endl;
                    return 0;
                }
            }
            else{
                string latest_hash = file_content(".store/branches/"+branch);
                if (latest_hash.length() != 64){
                    //cout << "HELLO" << endl;
                    cout << "HELLO";
                    cerr << __REPO__ERROR__ << endl;
                    return 0;
                }
                vector<string> vec;
                getTreeAndParent(vec,latest_hash);
                if (vec.size() == 0){
                    return 0;
                }
                string tree = vec[0];
                ifstream ifs(".store/commits/" + latest_hash.substr(0,2) + "/" + latest_hash);
                unordered_map<string,string> tree_blobs;
                unordered_map<string,string> working_blobs;
                get_tree_blobs(tree,tree_blobs);
                get_working_blobs(".",working_blobs);
                bool changes_there = false;
                for (auto x:tree_blobs){
                    if (valid_sha(x.second) == false){
                        show_error(__REPO__ERROR__);
                        return 0;
                    }
                    if (working_blobs.find(x.first) != working_blobs.end()){
                        if (valid_sha(working_blobs[x.first]) == false){
                            show_error(__REPO__ERROR__);
                            return 0;
                        }
                        if (working_blobs[x.first] != x.second){
                            changes_there = true;
                            cout << CYAN << "Modified file:" << NC << "\t" << x.first.substr(2) << endl;
                        }
                        working_blobs.erase(x.first);
                    }
                    else{
                        changes_there = true;
                        cout << RED << "Deleted file:" << NC << "\t" << x.first.substr(2) << endl;
                    }
                }
                for (auto x:working_blobs){
                    changes_there = true;
                    cout << GREEN << "Untracked file:" << NC << "\t" << x.first.substr(2) << endl;
                }
                if (changes_there == false){
                    cout << "working directory is clean" << endl;
                    return 0;
                }
            }
        }
        else if (argc==3){
            if (string(argv[2]) == "--help" || string(argv[2]) == "-h"){

            }
        }
    }
}
