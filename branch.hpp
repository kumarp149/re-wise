#ifndef __BRANCH__HEADER__
#define __BRANCH__HEADER__

#include <fstream>
#include <iostream>
#include <bits/stdc++.h>
#include <filesystem>

using namespace std;

namespace fs = std::filesystem;

using namespace fs;

string get_branch();

void get_all_branches(unordered_set<string>& branch_list);

int delete_branch(string branch);


#endif