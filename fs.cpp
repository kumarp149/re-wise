#include "fs.hpp"

void init_repo(string name,bool flag){
    path p1(".store");
    path p2(".store/HEAD");  //file
    path p3(".store/blobs");path p4(".store/commits");path p5(".store/trees");path p6(".store/branches");
    path p7(".store/user");   //file
    if (exists(p1) && exists(p2) && exists(p3) && exists(p4) && exists(p5) && exists(p6) && exists(p7) && flag==false){
        cout << "already a repo" << endl;
        return;
    }
    else{
        if (exists(p1)){
            remove_all(p1);create_directory(p1);create_directory(p3);create_directory(p4);create_directory(p5);
            create_directory(p6);
            ofstream out;
            out.open(".store/HEAD");out.close();
            out.open(".store/user");out << name;
            out.close();
        }
        else{
            create_directory(p1);create_directory(p3);create_directory(p4);create_directory(p5);create_directory(p6);
            ofstream out;
            out.open(".store/HEAD");out.close();
            out.open(".store/user");out << name;out.close();
            cout << GREEN << " initialized empty repo" << NC;
        }
    }
}

int replace_content(string path,string content){
    ofstream ofs(path,std::ofstream::out | std::ofstream::trunc);
    if (!ofs){
        show_error("There is a problem with the operation. Try again");
        return -1;
    }
    ofs << content;ofs.close();
    return 1;
}

string get_user(){
    ifstream ifs(".store/user");
    if (!ifs){
        cerr << "probably the repo is tampered. Please type \"out reinit --help\" to know more" << endl;
        return "";
    }
    string ret;
    ifs >> ret;
    return ret;
}

bool file_exist(string path){
    if (!exists(path)){
        return false;
    }
    return true;
}

bool is_repo(){
    path p1(".store");
    path p2(".store/HEAD");  //file
    path p3(".store/blobs");
    path p4(".store/commits");
    path p5(".store/trees");
    path p6(".store/branches");
    path p7(".store/user");   //file
    if (exists(p1) && exists(p2) && exists(p3) && exists(p4) && exists(p5) && exists(p6) && exists(p7)){
        return true;
    }
    return false;
}

void show_changes(string path,bool& first_commit,string latest_commit,int& changes){
    if (first_commit == true){
        for (auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it){
            if (it->symlink_status().type() == file_type::directory && it->path().string() != "./.store"){
                show_changes(it->path().string(),first_commit,"",changes);
            }
            else if (it->path().string() == "./.store"){
                continue;
            }
            else{
                changes++;
                cout << GREEN << "Untracked File:" << NC << "\t" << it->path().string().substr(2) << endl;
            }
        }
    }
}

//get_tree_blobs gives path to hash map of all the files present in the previous commit

int get_tree_blobs(string tree_hash,unordered_map<string,string>& tree_blobs){
    ifstream ifs(".store/trees/" + tree_hash.substr(0,2) + "/" + tree_hash);
    if (!ifs){
        return -1;
    }
    bool flag = false;
    for (string line; getline(ifs,line);){
        if (flag == false){
            flag = true;
            continue;
        }
        const char* pointer = line.data();
        //size_t size = line.size();
        //string decompressed_data = decompress(pointer,size);
        string hash = line.substr(0,64);
        string path = line.substr(64);
        tree_blobs[path] = hash;
    }
    return 1;
}

//get_working_blobs gives a path to hash map of all the files present in the working directory.

int get_working_blobs(string path,unordered_map<string,string>& working_blobs){
    for (auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it){
        if (it->symlink_status().type() == file_type::directory && it->path().string()!="./.store"){
            int val = get_working_blobs(it->path().string(),working_blobs);
        }
        else if (it->path().string() == "./.store"){
            continue;
        }
        else{
            working_blobs[it->path().string()] = file_sha(it->path().string());
        }
    }
    return 1;
}

string file_content(string path){
    std::ifstream t(path);
    if (!t){
        return __FILE__ERROR__;
    }
    std::stringstream buffer;
    buffer << t.rdbuf();
    return buffer.str();
}

string compress_string(string& s){
    const char * pointer = s.data();
    size_t sz = s.size();
    return gzip::compress(pointer,sz);
}

string decompress_string(string s){
    const char * compressed_pointer = s.data();
    std::string decompressed_data = gzip::decompress(compressed_pointer,s.size());
    return decompressed_data;
}

void show_error(string s){
    cerr << RED << "error: " << NC << s << endl;
}

void show_success(string prefix,string postfix){
    cout << RED << prefix  << NC << postfix << endl;
}

void getTreeAndParent(vector<string>& vec,string& latest_hash){
    //cout << latest_hash << endl;
    ifstream ifs(".store/commits/" + latest_hash.substr(0,2) + "/" + latest_hash);
    if (!ifs){
        cout << "FIRST" << endl;
        show_error(__REPO__ERROR__);
        return;
    }
    for (string line; getline(ifs,line);){
        vec.push_back(line);
    }
    if (vec[0].length() != 64){
        cout << "SECOND" << endl;
        show_error(__REPO__ERROR__);
        vec = {};
        return;
    }
    else if (vec.size()==2 && vec[1].length() != 0 && vec[1].length() != 64){
        cout << "THIRD" << endl;
        show_error(__REPO__ERROR__);
        vec = {};
        return;
    }
    return;
}

int create_blob_file(unordered_map<string,string>& working_blobs,string path){
    string hash = working_blobs[path];
    ifstream ifs(".store/blobs/" + hash.substr(0,2) + "/" + hash);
    if (!exists(".store/blobs/" + hash.substr(0,2) + "/")){
        create_directory(".store/blobs/" + hash.substr(0,2) + "/");
    }
    ofstream out(".store/blobs/" + hash.substr(0,2) + "/" + hash);
    string temp = file_content(path);
    out << compress_string(temp);
    out.close();
    return 0;
}

bool clean_repo(){
    string branch = file_content(".store/HEAD");
    string latest_hash = file_content(".store/branches/"+branch);
    vector<string> vec;
    getTreeAndParent(vec,latest_hash);
    string tree = vec[0];
    string parent = (vec.size() == 2) ? (vec[1]) : ("");
    unordered_map<string,string> tree_blobs, working_blobs;
    int val = get_tree_blobs(tree,tree_blobs);
    get_working_blobs(".",working_blobs);
    for (auto x:tree_blobs){
        if (working_blobs.find(x.first) != working_blobs.end()){
            if (working_blobs[x.first] != x.second){
                return false;
            }
            working_blobs.erase(x.first);
        }
        else{
            return false;
        }
    }
    if (working_blobs.size()>0){
        return false;
    }
    return true;
}

void change_tree(string from,string to){
    string latest_hash = file_content(".store/branches/"+to);
    vector<string> vec;
    getTreeAndParent(vec,latest_hash);
    string tree = vec[0];
    string parent = (vec.size() == 2) ? (vec[1]) : ("");
    unordered_map<string,string> tree_blobs, working_blobs;
    int val = get_tree_blobs(tree,tree_blobs);
    get_working_blobs(".",working_blobs);
    for (auto x:tree_blobs){
        if (working_blobs.find(x.first) != working_blobs.end()){
            if (working_blobs[x.first] != x.second){
                //file content different
                string blob_hash = x.second;
                cout << x.first << endl;
                string decompressed_content = decompress_string(file_content(".store/blobs/" + blob_hash.substr(0,2) + "/" + blob_hash));
                replace_content(x.first,decompressed_content);
            }
            working_blobs.erase(x.first);
        }
        else{
            //file in the tree_blobs is not found in the current branch
            string blob_hash = x.second;
            string decompressed_content = decompress_string(file_content(".store/blobs/" + blob_hash.substr(0,2) + "/" + blob_hash));
            replace_content(x.first,decompressed_content);
        }
    }
    for (auto x:working_blobs){
        //file exists only in the current branch
        remove(x.first);
    }
    replace_content(".store/HEAD",to);
}